import 'package:ayosemarang/main.dart';
import 'package:ayosemarang/pagebaru.dart';
import 'package:ayosemarang/utility/other.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

void main() => runApp(MultiProvider(providers: [], child: SearchList()));

class SearchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'ayo jakarta',
      debugShowCheckedModeBanner: false,
      home: new ExamplePage(),
    );
  }
}

class ExamplePage extends StatefulWidget {
  @override
  _ExamplePageState createState() => new _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List post_titles = new List();
  List filteredTitle = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = Image.asset('assets/logo.png',
      height: 500.0, width: 200.0, alignment: FractionalOffset.center);

  _ExamplePageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredTitle = post_titles;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
      resizeToAvoidBottomPadding: false,
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      backgroundColor: Colors.grey,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredTitle.length; i++) {
        if (filteredTitle[i]['post_title']
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredTitle[i]);
        }
      }
      filteredTitle = tempList;
    }
    return ListView.builder(
      itemCount: post_titles == null ? 0 : filteredTitle.length,
      itemBuilder: (BuildContext context, int index) {
        var listTile = new ListTile(
          trailing: new Image.network(
            "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('MM').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('dd').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${filteredTitle[index]['post_id']}/${filteredTitle[index]['post_image_content']}",
            fit: BoxFit.cover,
            height: 70.0,
            width: 70.0,
          ),
          title: Text(filteredTitle[index]['post_title']),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => PageBaru(
                      array: post_titles,
                      index: index,
                    )));
          },
          subtitle: Text(DateFormat('dd-MMMM-yyyy').format(
              DateTime.parse(filteredTitle[index]['post_date_created']))),
        );
        return new Card(
          child: listTile,
          margin: const EdgeInsets.all(5.0),
        );
      },
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = Image.asset('assets/logo.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center);
        filteredTitle = post_titles;
        _filter.clear();
      }
    });
  }

  void _getNames() async {
    final response =
        await dio.get('https://www.ayosemarang.com/api_mob_new/getSearch');
    List tempList = new List();
    for (int i = 0; i < response.data['data'].length; i++) {
      tempList.add(response.data['data'][i]);
    }
    setState(() {
      post_titles = tempList;
      post_titles.shuffle();
      filteredTitle = post_titles;
    });
  }
}
