import 'package:ayosemarang/detailpage.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:photo_view/photo_view.dart';
import 'package:share/share.dart';
import 'package:ayosemarang/utility/other.dart';

class PageBaru extends StatefulWidget {
  final List array;
  final int index;
  PageBaru({Key key, this.array, this.index}) : super(key: key);
  @override
  _PageBaruState createState() => _PageBaruState();
}

class _PageBaruState extends State<PageBaru> {
  List artikel;
  List artikelkait;
  get index => null;

  void testPost(String kode) {
    var url = "https://www.ayosemarang.com/api_mob_new/getArticle";
    http.post(url,
        headers: {"Accept": "application/json"},
        body: {"id": kode}).then((response) {
      var resBody = json.decode(response.body);
      setState(() {
        artikel = resBody["result"];
        artikelkait = resBody["newsCategory"]["data"];
      });
    }).catchError((err) {
      print(err);
    });
  }

  Widget _createHtmlView(BuildContext context) {
    return Html(
      data: artikel[0]["post_content"],
      padding: const EdgeInsets.only(
        left: 5.0,
      ),
      onLinkTap: (url) {
        String _url = url.substring(0, url.lastIndexOf("/")) + "";
        String id = _url.substring(_url.lastIndexOf("/") + 1);
        if (url.startsWith("https://www.ayosemarang.com/")) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => DetailPage(
                id: id,
                tag: "category_$id",
              ),
            ),
          );
        } else {
          _launchURL(url);
        }
      },
      useRichText: true,
      defaultTextStyle: TextStyle(fontSize: 16),
    );
  }

  String cetakURL(List array, int index) {
    String datar = array[index]["post_date"].substring(0, 10);
    String convertDate = datar.replaceAll("-", '/');
    String koder = array[index]["post_id"];
    String gambarr = array[index]["post_image_content"];
    String urlFinal =
        "https://www.ayosemarang.com/images-semarang/post/articles/"
        "$convertDate"
        "/"
        "$koder"
        "/"
        "$gambarr";
    return urlFinal;
  }

  Widget detailScreen() {
    return Scaffold(
        backgroundColor: Colors.black,
        body: GestureDetector(
          child: Center(
            child: Hero(
              tag: 'imageHero',
              child: PhotoView(
                imageProvider:
                    NetworkImage(cetakURL(widget.array, widget.index)),
              ),
            ),
          ),
        ));
  }

  String cetakEditor(List array, int index) {
    String editor = array[index]["editor"];
    print(editor);
    return editor;
  }

  String cetakTanggal(List array, int index) {
    String datar = array[index]["post_date"];
    print(formatDate(DateTime(1989, 2, 21), [yy, '-', M, '-', d]));

    String convertDate = datar.replaceAll("-", '/');
    return convertDate;
  }

  @override
  void initState() {
    super.initState();
  }

  Widget getLoading() {
    return new Scaffold(
        body: Center(
      child: Loading(
          indicator: BallPulseIndicator(), size: 80.0, color: Colors.grey),
    ));
  }

  Widget cekLoading() {
    if ((artikel == null) | (artikelkait == null)) {
      this.testPost(widget.array[widget.index]["post_id"]);
      return getLoading();
    } else {
      return mainFunction(context);
    }
  }

  Widget cetakWidget(List array, int index) {
    return new GestureDetector(
      onTap: () {
        setState(() {
          var route = new MaterialPageRoute(
            builder: (BuildContext context) => new PageBaru(
              array: array,
              index: index,
            ),
          );
          Navigator.of(context).push(route);
        });
      },
      child: Container(
        height: 100.0,
        padding: const EdgeInsets.only(
          top: 5.0,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(6.0),
              child: Image.network(
                cetakURL(array, index).toString(),
                height: 150.0,
                width: 130.0,
                fit: BoxFit.fill,
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(right: 5.0),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 1.0, left: 2.0),
                      child: SizedBox(
                        height: 38.0,
                        child: Text(
                          array[index]["post_title"],
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: RichText(
                      text: TextSpan(
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          array[index]["author"] == null
                              ? TextSpan()
                              : TextSpan(children: <TextSpan>[
                                  TextSpan(
                                    text: "Oleh ",
                                    style: TextStyle(
                                      fontSize: 11.0,
                                      color: Colors.black,
                                    ),
                                  ),
                                  TextSpan(
                                      text: array[index]["author"],
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 11.0,
                                        decoration: TextDecoration.none,
                                        fontWeight: FontWeight.bold,
                                      )),
                                ]),
                          TextSpan(
                            text: _getDateBetween(array[index]["post_date"]),
                            style: TextStyle(
                              fontSize: 10.0,
                              decoration: TextDecoration.none,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget mainFunction(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[400],
          centerTitle: true,
          title: Image.asset('assets/logo.png',
              height: 500.0, width: 200.0, alignment: FractionalOffset.center),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Share.share(
                "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(widget.array[widget.index]["post_date_created"]))}/${DateFormat('MM').format(DateTime.parse(widget.array[widget.index]["post_date_created"]))}/${DateFormat('dd').format(DateTime.parse(widget.array[widget.index]["post_date_created"]))}/${widget.array[widget.index]["post_id"]}/${widget.array[widget.index]["slug"]}");
          },
          child: Icon(Icons.share, color: Colors.white),
          backgroundColor: Colors.redAccent,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.all(10),
                child: new Text(
                  widget.array[widget.index]["post_title"],
                  style: TextStyle(
                    fontSize: 21,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(right: 10, left: 10, bottom: 10),
                child: Container(
                  child: Row(
                    children: [
                      Column(
                        children: [
                          new Text("Ayo Semarang",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(right: 10, left: 10, bottom: 10),
                child: Container(
                  child: Row(
                    children: [
                      Column(
                        children: [
                          new Text(
                              "${DateFormat.yMMMMEEEEd().format(DateTime.parse(artikel[0]['post_date']))} ${DateFormat("Hms").format(DateTime.parse(artikel[0]['post_date']))} WIB"),
                        ],
                      ),
                      Column(
                        children: [
                          new Text("|"),
                        ],
                      ),
                      Column(
                        children: [
                          new Text(artikel[0]["category_name"],
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              new Container(
                height: 250.0,
                child: GestureDetector(
                  child: Hero(
                    tag: 'imageHero',
                    child: Image.network(
                      cetakURL(widget.array, widget.index),
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                      return detailScreen();
                    }));
                  },
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(left: 10, top: 0, bottom: 5.0),
                child: new Text(
                  artikel[0]["post_image_caption"],
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              Divider(
                height: 3.0,
                color: Colors.black45,
              ),
              _createHtmlView(context),
              _createTitle("Berita Terkait"),
              new SizedBox(height: 10),
              cetakWidget(artikelkait, 0),
              cetakWidget(artikelkait, 1),
              cetakWidget(artikelkait, 2),
              cetakWidget(artikelkait, 3),
              cetakWidget(artikelkait, 4),
              cetakWidget(artikelkait, 5),
              cetakWidget(artikelkait, 6),
              cetakWidget(artikelkait, 7),
              cetakWidget(artikelkait, 8),
              cetakWidget(artikelkait, 9),
            ],
          ),
        ));
  }

  Widget _createTitle(String title) {
    return Container(
      margin: EdgeInsets.only(top: 25.0, bottom: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0, left: 10.0),
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Could not launch $url";
    }
  }

  @override
  Widget build(BuildContext context) {
    return cekLoading();
  }
}
