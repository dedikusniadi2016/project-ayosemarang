import 'package:ayosemarang/DetailAboutPage.dart';
import 'package:ayosemarang/model/about.dart';
import 'package:ayosemarang/search.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'pageumum.dart' as umum;
import 'pagenetizen.dart' as netizen;
import 'pagebisnis.dart' as bisnis;
import 'pagepsis.dart' as psis;
import 'pagependik.dart' as pendidikan;
import 'pagewisata.dart' as wisata;
import 'pagesemarang.dart' as semarang;
import 'pagedaerah.dart' as daerah;
import 'homepage.dart' as homepage;
import 'pagefoto.dart' as foto;
import 'pageolahraga.dart' as olaharaga;
import 'pageVideo.dart' as video;
import 'indexpage.dart' as index;
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:dio/dio.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MaterialApp(
    home: TestData(),
    debugShowCheckedModeBanner: false,
  ));
}

class TestData extends StatefulWidget {
  @override
  TestDataState createState() => TestDataState();
}

class TestDataState extends State<TestData>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  List kategori;
  AboutModel _aboutModel;
  bool _status = false;

  Future<String> getCategory() async {
    var response = await http.get(
        Uri.encodeFull("https://www.ayosemarang.com/api_mob_new/getKanal"),
        headers: {"Accept": "application/json"});

    var resBody = json.decode(response.body);
    setState(() {
      kategori = resBody["category"];
    });
  }

  TabController controller;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    controller = new TabController(vsync: this, length: 13);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();

    controller.dispose();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      print("You are not connected to internet");
    }
  }

  Widget getLoading() {
    return new Scaffold(
        body: Center(
      child: Loading(
          indicator: BallPulseIndicator(), size: 80.0, color: Colors.grey),
    ));
  }

  Widget cekLoading() {
    if ((kategori == null)) {
      getCategory();
      return getLoading();
    } else {
      return mainFunction();
    }
  }

  Future<AboutModel> _fetchGetAboutUs() async {
    Response response =
        await Dio().get("https://www.ayosemarang.com/api_mob_new/getPages");

    var decodeJson = jsonDecode(response.toString());
    if (identical(AboutModel.fromJson(decodeJson).kode, 200)) {
      _aboutModel = AboutModel.fromJson(decodeJson);
    }

    debugPrint(_aboutModel.data[2].content);

    return _aboutModel;
  }

  Widget _futureBuilder() {
    return FutureBuilder(
        future: _fetchGetAboutUs(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return _aboutModel == null
                  ? Container(
                      child: Center(
                        child: Text(
                          "data tidak ditemukan",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : _buildListView(snapshot.data, context);
            default:
              if (snapshot.hasError)
                return Center(child: Text("Error : ${snapshot.error}"));
          }
        });
  }

  Widget _buildListView(AboutModel data, BuildContext context) => ListView(
        children:
            data.data.map<Widget>((item) => _buildItem(item, context)).toList(),
      );

  _buildItem(Data item, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) =>
                DetailsAboutPage(title: item.title, text: item.content)));
      },
      child: SizedBox(
        child: Stack(
          children: <Widget>[
            ListTile(
              title: Text(item.title),
            ),
          ],
        ),
      ),
    );
  }

  Widget mainFunction() {
    return new Scaffold(
      drawer: Drawer(
        child: _status == true
            ? _aboutModel == null
                ? _futureBuilder()
                : _buildListView(_aboutModel, context)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.grey[400],
        centerTitle: true,
        title: Image.asset('assets/logo.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchList()),
              );
            },
          ),
        ],
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          new homepage.TestData(),
          new semarang.PageSemarang(),
          new daerah.PageDaerah(),
          new umum.PageUmum(),
          new psis.PagePsis(),
          new olaharaga.PageOlahraga(),
          new wisata.PageWisata(),
          new bisnis.PageBisnis(),
          new pendidikan.PagePendidikan(),
          new netizen.PageNetizen(),
          new foto.PhotoGalleryPage(),
          new video.VideoGalleryPage(),
          new index.IndexPage(),
        ],
      ),
      bottomNavigationBar: new TabBar(
        controller: controller,
        isScrollable: true,
        tabs: <Widget>[
          new Tab(
            child: Text(
              "Terbaru",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Semarang",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Daerah",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Umum",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "PSIS",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Olahraga",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Wisata",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Bisnis",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Pendidikan",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "Netizen",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "foto",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(
            child: Text(
              "video",
              style: TextStyle(color: Colors.black),
            ),
          ),
          new Tab(child: Text("Index",
          style: TextStyle(color: Colors.black),
          ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return cekLoading();
  }
}
