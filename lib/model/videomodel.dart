class VideoModel {
  String _status;
  int _kode;
  List<Data> _data;
  String _top;
  String _toplink;
  String _bottom;
  String _bottomlink;

  VideoModel(
      {String status,
        int kode,
        List<Data> data,
        String top,
        String toplink,
        String bottom,
        String bottomlink}) {
    this._status = status;
    this._kode = kode;
    this._data = data;
    this._top = top;
    this._toplink = toplink;
    this._bottom = bottom;
    this._bottomlink = bottomlink;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;
  String get top => _top;
  set top(String top) => _top = top;
  String get toplink => _toplink;
  set toplink(String toplink) => _toplink = toplink;
  String get bottom => _bottom;
  set bottom(String bottom) => _bottom = bottom;
  String get bottomlink => _bottomlink;
  set bottomlink(String bottomlink) => _bottomlink = bottomlink;

  VideoModel.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
    _top = json['top'];
    _toplink = json['toplink'];
    _bottom = json['bottom'];
    _bottomlink = json['bottomlink'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    data['top'] = this._top;
    data['toplink'] = this._toplink;
    data['bottom'] = this._bottom;
    data['bottomlink'] = this._bottomlink;
    return data;
  }
}

class Data {
  String _videoId;
  String _date;
  String _title;
  String _slug;
  String _youtube;
  String _source;
  String _video;
  String _editor;

  Data(
      {String videoId,
        String date,
        String title,
        String slug,
        String youtube,
        String source,
        String video,
        String editor}) {
    this._videoId = videoId;
    this._date = date;
    this._title = title;
    this._slug = slug;
    this._youtube = youtube;
    this._source = source;
    this._video = video;
    this._editor = editor;
  }

  String get videoId => _videoId;
  set videoId(String videoId) => _videoId = videoId;
  String get date => _date;
  set date(String date) => _date = date;
  String get title => _title;
  set title(String title) => _title = title;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get youtube => _youtube;
  set youtube(String youtube) => _youtube = youtube;
  String get source => _source;
  set source(String source) => _source = source;
  String get video => _video;
  set video(String video) => _video = video;
  String get editor => _editor;
  set editor(String editor) => _editor = editor;

  Data.fromJson(Map<String, dynamic> json) {
    _videoId = json['video_id'];
    _date = json['date'];
    _title = json['title'];
    _slug = json['slug'];
    _youtube = json['youtube'];
    _source = json['source'];
    _video = json['video'];
    _editor = json['editor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['video_id'] = this._videoId;
    data['date'] = this._date;
    data['title'] = this._title;
    data['slug'] = this._slug;
    data['youtube'] = this._youtube;
    data['source'] = this._source;
    data['video'] = this._video;
    data['editor'] = this._editor;
    return data;
  }
}
