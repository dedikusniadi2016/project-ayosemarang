import 'package:flutter/material.dart';
import 'pagebaru.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';


void main() {
  runApp(MaterialApp(home: PageDaerah()));
}

class PageDaerah extends StatefulWidget {
  @override
  _PageDaerahState createState() => _PageDaerahState();
}

class _PageDaerahState extends State<PageDaerah> {
  List daerah;
  String urlAd1;
  String urlAd2;
  String urlAd3;
  String urlAd4;
  String urlAd5;
  String adLink1;
  String adLink2;
  String adLink3;
  String adLink4;
  String adLink5;

  Future<String> getIklan() async {
    var response = await http.get(
        Uri.encodeFull("https://www.ayosemarang.com/api_mob_new/getAll"),
        headers: {"Accept": "application/json"});

    var resBody = json.decode(response.body);
    setState(() {
      urlAd1 = resBody["image_ads_1"];
      urlAd2 = resBody["image_ads_2"];
      urlAd3 = resBody["image_ads_3"];
      urlAd4 = resBody["image_ads_4"];
      urlAd5 = resBody["image_ads_5"];
      adLink1 = resBody["ads_link_1"];
      adLink2 = resBody["ads_link_2"];
      adLink3 = resBody["ads_link_3"];
      adLink4 = resBody["ads_link_4"];
      adLink5 = resBody["ads_link_5"];
    });
  }

  Widget cetakIklan(String urlgambar, String url) {
    return new Container(
      child: new InkWell(
        child: new Image.network(
          urlgambar,
          height: 60,
          width: MediaQuery.of(context).size.width,
        ),
        onTap: () => launch(url),
      ),
    );
  }

  void testdaerah() {
    var url = "https://www.ayosemarang.com/api_mob_new/getRecentArticle";
    http.post(url, headers: {
      "Accept": "application/json"
    }, body: {
      "cat_id": 112.toString(),
      "limit": 15.toString()
    }).then((response) {
      var resBody = json.decode(response.body);
      setState(() {
        daerah = resBody["data"];
      });
    }).catchError((err) {
      print(err);
    });
  }

  String cetakURL(List array, int index) {
    String datar = array[index]["post_date"].substring(0, 10);
    String convertDate = datar.replaceAll("-", '/');
    String koder = array[index]["post_id"];
    String gambarr = array[index]["post_image_content"];
    String urlFinal =
        "https://www.ayosemarang.com/images-semarang/post/articles/"
        "$convertDate"
        "/"
        "$koder"
        "/"
        "$gambarr";
    return urlFinal;
  }

  @override
  void initState() {
    super.initState();
  }

 
  Widget getLoading() {
    return new Scaffold(
        body: Center(
      child: Loading(indicator: BallPulseIndicator(), size: 80.0, color: Colors.grey),
    ));
  }

  Widget cekLoading() {
    if ((daerah == null) | (urlAd1 == null)) {
      getIklan();
      testdaerah();
      return getLoading();
    } else {
      return mainFunction();
    }
  }

  Widget cetakWidget(List array, int index) {
    return new GestureDetector(
      onTap: () {
        setState(() {
          var route = new MaterialPageRoute(
            builder: (BuildContext context) => new PageBaru(
              array: array,
              index: index,
            ),
          );
          Navigator.of(context).push(route);
        });
      },
      child: new Container(
          child: new Column(
        children: <Widget>[
          Align(
              alignment: Alignment.center,
              child: Image.network(
                cetakURL(array, index),
                fit: BoxFit.fill,
                width: MediaQuery.of(context).size.width,
                height: 250,
              )),
          Align(
            alignment: Alignment.bottomCenter,
            child: new Container(
                color: Colors.white,
                height: 100,
                child: new Padding(
                    padding: EdgeInsets.all(10),
                    child: new Column(
                      children: <Widget>[
                        Text(
                          array[index]["post_title"],
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                          ),
                        ),
                        SizedBox(height: 5),
                        Align(
                          alignment: Alignment.centerLeft,
                          child:
                              Text(_getDateBetween(array[index]["post_date"]),
                                  style: TextStyle(
                                    color: Colors.grey[400],
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12,
                                  )),
                        ),
                        SizedBox(
                          height: 5,
                        )
                      ],
                    ))),
          )
        ],
      )),
    );
  }

  Widget mainFunction() {
    return new Scaffold(
        body: Scaffold(
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                expandedHeight: 350,
                floating: false,
                pinned: false,
                flexibleSpace: FlexibleSpaceBar(
                  background: new Container(
                    child: cetakWidget(daerah, 0),
                  ),
                ),
              ),
              new SliverList(
                  delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return new GestureDetector(
                    onTap: () {
                      setState(() {
                        var route = new MaterialPageRoute(
                          builder: (BuildContext context) => new PageBaru(
                            array: daerah,
                            index: index,
                          ),
                        );
                        Navigator.of(context).push(route);
                      });
                    },
                    child: Container(
                      height: 100.0,
                      padding: const EdgeInsets.only(
                        top: 5.0,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(6.0),
                           
                              child: Image.network(
                                cetakURL(daerah, index).toString(),
                                height: 150.0,
                                width: 130.0,
                                fit: BoxFit.fill,
                              ),
                            
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 2.0, left: 0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        margin:
                                            const EdgeInsets.only(right: 5.0),
                                      ),
                                      GradientText(
                                        daerah[index]["category_name"],
                                        gradient: LinearGradient(colors: [
                                          Colors.orange,
                                          Colors.deepOrange,
                                          Colors.red
                                        ]),
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 1.0, left: 2.0),
                                    child: SizedBox(
                                      height: 38.0,
                                      child: Text(
                                        daerah[index]["post_title"],
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 10.0),
                                  child: RichText(
                                    text: TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        daerah[index]["author"] == null
                                            ? TextSpan()
                                            : TextSpan(children: <TextSpan>[
                                                TextSpan(
                                                  text: "Oleh ",
                                                  style: TextStyle(
                                                    fontSize: 11.0,
                                                  ),
                                                ),
                                                TextSpan(
                                                    text: daerah[index]
                                                        ["author"],
                                                    style: TextStyle(
                                                      fontSize: 11.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                              ]),
                                        TextSpan(
                                          text: _getDateBetween(
                                              daerah[index]["post_date"]),
                                          style: TextStyle(
                                            fontSize: 11.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                childCount: 15,
              ))
            ],
          ),
        ],
      ),
    ));
  }
  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget build(BuildContext context) {
    return cekLoading();
  }
}
