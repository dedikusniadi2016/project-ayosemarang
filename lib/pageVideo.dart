import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:ayosemarang/utility/other.dart';
import 'package:ayosemarang/model/videomodel.dart';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';

const KEY = "AIzaSyC_ZpIzTdTFQGRJw2iQudgOA2s5ZzdI-kk";

class VideoGalleryPage extends StatefulWidget {
  @override
  _VideoGalleryPageState createState() => _VideoGalleryPageState();
}

class _VideoGalleryPageState extends State<VideoGalleryPage> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  VideoModel _video;
  int _totalVideo = 0;
  int _pageVideo = 0;

  Future<VideoModel> _fetchGetVideo() async {
    Response response = await Dio().post(
      "https://www.ayosemarang.com/api_mob_new/getRecentVideo",
      data: FormData.fromMap({"page": _pageVideo, "limit": 30}),
    );

    var decodeJson = jsonDecode(response.toString());

    if (identical(VideoModel.fromJson(decodeJson).kode, 200)) {
      _video == null
          ? _video = VideoModel.fromJson(decodeJson)
          : _video.data.addAll(VideoModel.fromJson(decodeJson).data);

      setState(() {
        _totalVideo = _video.data.length;
      });
    }

    return _video;
  }

  Future<VideoModel> _handleRefreshVideo() async {
    _pageVideo = 0;
    Response response = await Dio().post(
      "https://www.ayosemarang.com/api_mob_new/getRecentVideo",
      data: FormData.fromMap({"page": _pageVideo, "limit": 30}),
    );

    var decodeJson = jsonDecode(response.toString());

    if (identical(VideoModel.fromJson(decodeJson).kode, 200)) {
      _video = VideoModel.fromJson(decodeJson);

      setState(() {
        _totalVideo = _video.data.length;
      });
    }

    return _video;
  }

  _playVideo(String id) {
    FlutterYoutube.playYoutubeVideoById(
      apiKey: KEY,
      videoId: id,
      autoPlay: true,
      fullScreen: false,
    );
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget _buildFutureView() => FutureBuilder(
      future: _fetchGetVideo(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _createListView(_video);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  Widget _createListView(VideoModel video) {
    return RefreshIndicator(
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: _totalVideo,
            itemBuilder: (BuildContext context, int index) {
              if (_status) {
                if (index >= _totalVideo - 1) {
                  _pageVideo++;
                  _fetchGetVideo();
                }
              } else {
                final snackBar =
                    SnackBar(content: Text("Koneksi internet terputus"));
                Scaffold.of(context).showSnackBar(snackBar);
              }

              return Container(
                child: GestureDetector(
                    onTap: () {
                      _playVideo(video.data[index].video);
                    },
                    child: _buildListItem(_video.data[index])),
              );
            }),
        onRefresh: _handleRefreshVideo);
  }

  Widget _cacheNetworkImage(String imageUrl, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: BoxFit.fill,
      );

  Widget _buildListItem(Data item) => Container(
       
        child: Stack(
          children: <Widget>[
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 60.0),
                child: Container(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(
                        'https://img.youtube.com/vi/${item.video}/mqdefault.jpg',
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                  height: 200,
                ),
              ),
            ),
            Positioned(
              top: 160,
              right: 10,
              left: 10,
              height: 80,
              child: Card(
                child: ListTile(
                  title: Text(item.title,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3),
                ),
              ),
            ),
            Positioned(
              top: 210,
              right: 100,
              left: 30,
              child: item.editor == null
                  ? Container()
                  : Row(
                      children: <Widget>[
                        Text(
                          "Oleh : ",
                          style: TextStyle(fontSize: 11.0),
                        ),
                        Text(
                          item.editor.toUpperCase(),
                          style: TextStyle(
                            fontSize: 11.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
            ),
            Positioned(
              top: 215,
              right: 100,
              left: 30,
              child: Container(
                margin: const EdgeInsets.only(
                  top: 8.0,
                ),
                child: Text(_getDateBetween(item.date),
                    style: TextStyle(fontSize: 11.0)),
              ),
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    // return Scaffold(
    //   key: _scaffoldKey,
    //   body: _status == true
    //       ? _video == null ? _buildFutureView() : _createListView(_video)
    //       : Container(
    //           child: Center(
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: <Widget>[
    //                 Icon(Icons.signal_wifi_off),
    //                 Text(
    //                   "Connection Failed",
    //                   style: TextStyle(fontFamily: 'Montserrat'),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    // );
            return cekLoading();

  }

  Widget mainFunction() {
    return Scaffold(
      key: _scaffoldKey,
      body: _status == true
          ? _video == null ? _buildFutureView() : _createListView(_video)
          : Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.signal_wifi_off),
                    Text(
                      "Connection Failed",
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

   Widget getLoading() {
    return new Scaffold(
        body: Center(
      child: Loading(
          indicator: BallPulseIndicator(), size: 80.0, color: Colors.grey),
    ));
  }

  Widget cekLoading() {
    if ((_video == null)) {
      
      _handleRefreshVideo();
      return getLoading();
    } else {
      return mainFunction();
    }
  }


  

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      print("You are not connected to internet");
    }
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
