class Other{
  static const URL_IMAGE = "https://www.ayosemarang.com/images-semarang/post/articles/";
  static const URL_PHOTOS = "https://www.ayosemarang.com/images-semarang/post/photos/";
  static const URL_VIDEO = "https://www.ayosemarang.com/images-semarang/watch/";
  static const BASE_URL = "https://www.ayosemarang.com/api_mob_new/";
  static const URL_WEBSITE = "https://www.ayosemarang.com/read/";
  static const URL_READ_PHOTO = "https://www.ayosemarang.com/view/";
}