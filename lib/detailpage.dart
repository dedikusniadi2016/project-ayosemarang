import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:ayosemarang/model/artikel_detail.dart';
import 'package:ayosemarang/utility/other.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:photo_view/photo_view.dart';
import 'package:share/share.dart';

class DetailPage extends StatefulWidget {
  final String id;
  final String tag;
  String image;
  String category;

  DetailPage({this.id, this.tag, this.image, this.category});

  @override
  _ContentPageState createState() => _ContentPageState();
}

enum TtsState { playing, stopped }

class _ContentPageState extends State<DetailPage>
    with TickerProviderStateMixin {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  ArticleDetails _details;

  String _image;
  String _category;
  bool _collapse = false;
  Related artikelkait;
  Related artikel;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  AnimationController _controller;
  Animation<double> _animation;

  Related article;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    _controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _controller.forward();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  Future<ArticleDetails> _fetchGetArticle() async {
    print("id " + widget.id);
    Response response = await Dio().post(
      "${Other.BASE_URL}getArticle",
      data: FormData.fromMap({"id": widget.id}),
    );
    var decodeJson = jsonDecode(response.toString());
    if (identical(ArticleDetails.fromJson(decodeJson).kode, 200)) {
      _details = ArticleDetails.fromJson(decodeJson);

      if (widget.image == null) {
        setState(() {
          widget.image =
              "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].postImageContent}";

          widget.category = _details.result[0].categoryName;
        });
      }
    }

    print(_details);

    return _details;
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget _createHtmlView(BuildContext context) {
    return Html(
      data: "<p align=\"justify\">${_details.result[0].postContent}</p>",
      padding: const EdgeInsets.only(
        left: 5.0,
      ),
      onLinkTap: (url) {
        String _url = url.substring(0, url.lastIndexOf("/")) + "";
        String id = _url.substring(_url.lastIndexOf("/") + 1);
        if (url.startsWith("https://www.ayosemarang.com/")) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => DetailPage(
                id: id,
                tag: "category_$id",
              ),
            ),
          );
        } else {
          _launchURL(url);
        }
      },
      useRichText: true,
      defaultTextStyle: TextStyle(fontSize: 16),
    );
  }

  Widget _futureBuilderContent(BuildContext context) => FutureBuilder(
      future: _fetchGetArticle(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _details == null
                ? Container(
                    child: Center(
                      child: Text(
                        "data tidak ditemukan",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : _createListContent(context);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  Widget _buildContentView(BuildContext context) => NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          _collapse = innerBoxIsScrolled;
          return <Widget>[];
        },
        body: _status == true
            ? _futureBuilderContent(context)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      );

  _createListViewRelated(Related article, BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: article.data.length,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => DetailPage(
                              id: article.data[index].postId,
                              tag: "category_$index",
                              category: article.data[index].categoryName,
                              image:
                                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                            )));
                  },
                  child: Hero(
                      tag: "category_$index",
                      child: _buildListItem(article.data[index], context)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  _createListViewMust(NewsCategory article, BuildContext context) {
    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      children: article.data
          .map((item) => Padding(
                padding: EdgeInsets.only(left: 2.0, right: 2.0),
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => DetailPage(
                                  id: item.postId,
                                  tag: "category_${item.postId}",
                                  category: item.categoryName,
                                  image:
                                      "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                                )));
                      },
                      child: Hero(
                          tag: "category_${item.postId}",
                          child: _buildListItem(item, context)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Divider(
                        height: 1.0,
                        color: Colors.black26,
                      ),
                    ),
                  ],
                ),
              ))
          .toList(),
    );
  }

  Widget _buildListItem(Data item, BuildContext context) => Material(
        child: Container(
          height: 100.0,
          child: Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Image.network(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                    height: 150.0,
                    width: 130.0,
                    fit: BoxFit.fill,
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 1.0),
                          child: SizedBox(
                            height: 38.0,
                            child: Text(
                              item.postTitle,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              item.author == null
                                  ? TextSpan()
                                  : TextSpan(children: <TextSpan>[
                                      TextSpan(
                                        text: "Oleh ",
                                        style: TextStyle(
                                          fontSize: 11.0,
                                        ),
                                      ),
                                      TextSpan(
                                          text: "${item.author} ",
                                          style: TextStyle(
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.bold,
                                          )),
                                    ]),
                              TextSpan(
                                text: _getDateBetween(item.postDate),
                                style: TextStyle(
                                  fontSize: 11.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  Widget cetakWidget(Related article, int index) {
    return new GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => DetailPage(
                  id: article.data[index].postId,
                  tag: "category_$index",
                  category: article.data[index].categoryName,
                  image:
                      "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                )));
      },
      child: Container(
        height: 100.0,
        padding: const EdgeInsets.only(
          top: 5.0,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(right: 5.0),
                        ),
                        GradientText(
                          article.data[index].categoryName,
                          gradient: LinearGradient(colors: [
                            Colors.orange,
                            Colors.deepOrange,
                            Colors.red
                          ]),
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 1.0, left: 2.0),
                      child: SizedBox(
                        height: 38.0,
                        child: Text(
                          article.data[index].postTitle,
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: RichText(
                      text: TextSpan(
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          article.data[index].author == null
                              ? TextSpan()
                              : TextSpan(children: <TextSpan>[
                                  TextSpan(
                                    text: "Oleh ",
                                    style: TextStyle(
                                      fontSize: 11.0,
                                    ),
                                  ),
                                  TextSpan(
                                      text: article.data[index].author,
                                      style: TextStyle(
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.bold,
                                      )),
                                ]),
                          TextSpan(
                            text: _getDateBetween(
                              article.data[index].postDate,
                            ),
                            style: TextStyle(
                              fontSize: 11.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(6.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                  height: 110.0,
                  width: 100.0,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget detailScreen() {
    return Scaffold(
      backgroundColor: Colors.black,
      body: GestureDetector(
        child: Center(
          child: Hero(
            tag: 'imageHero',
            child: PhotoView(
                imageProvider: NetworkImage(
              "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].postImageContent}",
            )),
          ),
        ),
      ),
    );
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 2),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  _createListContent(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.all(10),
          child: new Text(
            _details.result[0].postTitle,
            style: TextStyle(
              fontSize: 21,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        new Padding(
          padding: EdgeInsets.only(right: 10, left: 10, bottom: 10),
          child: Container(
            child: Row(
              children: [
                Column(
                  children: [
                    new Text("Ayo Semarang",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                  ],
                ),
              ],
            ),
          ),
        ),
        new Padding(
          padding: EdgeInsets.only(right: 10, left: 10, bottom: 10),
          child: Container(
            child: Row(
              children: [
                Column(
                  children: [
                    new Text(
                        "${DateFormat.yMMMMEEEEd().format(DateTime.parse(_details.result[0].postDate))} ${DateFormat("Hms").format(DateTime.parse(_details.result[0].postDate))} WIB"),
                  ],
                ),
                Column(
                  children: [
                    new Text("|"),
                  ],
                ),
                Column(
                  children: [
                    new Text(_details.result[0].categoryName,
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold))
                  ],
                ),
              ],
            ),
          ),
        ),
        new Container(
          height: 250.0,
          child: GestureDetector(
            child: Hero(
              tag: 'imageHero',
              child: Image.network(
                "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].postImageContent}",
              ),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return detailScreen();
              }));
            },
          ),
        ),
        Container(
            margin: EdgeInsets.only(bottom: 15.0),
            child: Text(
              _details.result[0].postImageCaption,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.0),
            )),
        Divider(
          height: 3.0,
          color: Colors.black45,
        ),
        _createHtmlView(context),
        _createTitle("Berita Terkait"),
        identical(_details.newsCategory.kode, 502) == true
            ? Container()
            : identical(_details.newsCategory.kode, 502) == true
                ? Container()
                : Container(
                    height:
                        (_details.newsCategory.data.length * 106).toDouble(),
                    child: _createListViewMust(_details.newsCategory, context),
                  ),
      ],
    );
  }

  Widget _createTitle(String title) {
    return Container(
      margin: EdgeInsets.only(top: 25.0, bottom: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0, left: 10.0),
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Could not launch $url";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[400],
        centerTitle: true,
        title: Image.asset('assets/logo.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Share.share(
              "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].slug}");
        },
        child: Icon(Icons.share, color: Colors.white),
        backgroundColor: Colors.redAccent,
      ),
      key: _scaffoldKey,
      body: _buildContentView(context),
    );
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
